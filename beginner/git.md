---
theme: uncover
class: invert
title: So What's a Git?
---

## Git Windows Install Manual

<https://maglit.me/yemop>

## Also Make an Account Here (One or Both)

<https://about.gitlab.com/>

<https://github.com/>

<!-- Shown while people are coming in before we start at 9:15 -->

---

## So What's a Git?

By David Krauthamer

---

## It's not GitHub or GitLab

<!--
These sites use git as their version control backend, but the program git itself
is entirely separate from either of these companies.
-->

---

## Version Control System

<!--
To give a bit of time while you guys are installing stuff I'm going to go
over the different types of version control systems
-->

---

## Local

Keeps track of changes made to files, and can revert at any point.

<!--
Doesn't have to just be plain text, can also be pictures, data outputs,
part files for cad software, etc. The downside is if you haven't backed up the
version control information somewhere, if you lose it its all gone permanently.
-->

---

## Centralized

One repository on a server, everybody commits to that.

<!--
If the copy of the project goes down or is corrupted, you can't access the code
and retrieve previous commits. Remote commits are slow and require a connection
to the internet.
-->

---

## Distributed

![Distributed VCS in a nutshell](./distributed_meme.png)

<!--
You have a local copy, you make changes and commit, you push your commits
to the remote, others pull your commits, then they fix any merge conflicts they
have. You get to do the whole process too :)

There's no longer the single point of failure, commits can be done offline, and
merging and branching are done on the local side before being pushed to a remote
(we'll touch a bit on these later).
-->

---

### I'd Love One, Where Can I Get It?

<https://git-scm.com/downloads>

<https://about.gitlab.com/>

<https://github.com/>

<!--
If you missed it earlier, here are the links to care about again, download
the first one, and make an account on one or both of the second and third. If
you go for github, make sure to use your stevens email as they have "pro"
benefits for students (dunno what they actually are, but they're there).
-->

---

## Any Questions?

<!-- Eboard is here to help with installs / making accounts. -->

---

## Making a Local Repository

```bash
pwd
mkdir some_folder
cd some_folder
git init
ls -la
git status
```

```text
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```

<!--
pwd shows where you are in relation to the filesystem, useful if you want to
find the same location in the GUI file browser.

mkdir makes a folder with the name supplied. be careful with spaces in folder
names as that will be interpreted as making two separate folders. if you must
have space in the name, surround the name with quotes ""

cd changes into the directory you made

git init initializes git in the current working directory

ls -la will show all contents of a folder, including hidden files, which lets
us see the .git folder. version control info is stored in that folder, so don't
delete it.

git status will confirm that we are in a version controlled folder if it prints
the following message
-->

---

### Adding a File to Version Control

---

Create a Text File However You Like

```bash
notepad my_file.txt
ls
git status
```

```text
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include what will be committed)
      my_file.txt

nothing added to commit but untracked files present (use "git add" to track)
```

<!--
runs notepad to create a file, put whatever you like in there, save the file, and
close notepad

ls will show the newly created file

git status shows that git knows there's a new file / changes, but we haven't
staged them to be committed yet.
-->

---

### Stage Changes For The Next Commit

```bash
# stage changes from an individual file 
git add my_file.txt
# stage all changes in this folder, be careful doing this!
git add .
git status
```

```text
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
      new file: my_file.txt
```

---

## So Far So Good?

---

## Making a Commit

```bash
# will open your default editor to edit the commit message
# which should be notepad unless you changed it
git commit
# or -m specifies the commit message
git commit -m "Initial Commit"
```

```text
[<branch name> <commit hash>] <commit message>
<insertions, deletions, etc>
```

<!--
when naming commits you usually want to do it in the form of:
this commit will: <your commit contents here>

there's also other ways to name commits based on what it does, for example
starting with chore, fix, etc. depending on how organized the repository is
-->

---

## The gitignore File

```bash
# ignore all files with an extension
*.elf
# ignore any file containing ".so." in the name
*.so.*
# ignore the chicken folder and all files within
chicken/
# ignore all files in the chimkin folder, but not the folder itself
chimkin/*
# DO NOT ignore files in the chimkin folder ending with .egg
!chimkin/*.egg
# ignore the file hello.py in the project root
hello.py
# ignore the file bye.py in the src/ folder
src/bye.py
```

<https://www.toptal.com/developers/gitignore/>

<!--
The .gitignore file is used to tell git to well... ignore certain files that you
don't want committed to version control. This includes things like executables
generated from code, configuration files, and often times most importantly files
which contain secrets like passwords and api keys. There are certainly more rules
than what I've shown here, but you should be find in most situations with these.

Go ahead and create your own if you like with whatever rules you feel like. If
something has already been committed to version control, it will not be forgotten
if then added to the gitignore file. Just like any other file, the .gitignore
file can and should be version controlled as well, so add it and make a second
commit.

Finally, if you're doing a code related project, check out the website I've
linked, they have lots of templates to start off with for a given project type
so that you don't have to make an entire gitignore yourself.
-->

---

## Backing Things Up

---

## Make a Remote Repository

Head over to GitHub or GitLab and make a new repository with whatever name you
like.

If it asks you to put a README or anything like that by default, decline to do
so.

<!--
worth noting, github is owned by microsoft, so everything kept there can be
yoinked at their whims. additionally, any public code will be processed for
github copilot, which is an ai that tries to write code for you. I don't like or
trust microsoft, and you shouldn't either, however having a github account will
be needed later for some classes (CPE and EE I'm looking at you guys), and if
you want to contribute to open source the majority of projects are hosted there.
All of my personal projects are hosted on GitLab, and anything stevens related is
on github
-->

---

![w:1000px](./github_green_new_button.png)

You want the green new button on the left, or the "+" icon in the top right.

---

![h:650px](./new_repo_page.png)

<!-- Make sure that adding a readme is unchecked, gitignore template is none,
and license is none. -->

---

## Pushing Changes to a Remote

```bash
git remote add origin git@github.com:username/reponame.git
git remote add origin https://gitlab.com/username/reponame.git

git push -u origin master
# all previous commands only need to be run once unless you want to
# change / add a new remote, or change the default remote that gets
# pushed to
# use the following from now on
git push
```

<!--
The first two commands are adding a remote to the repository. This tells
git that there's a url you can yeet a copy of this repositry into and you're
allowed to do it.

If you know what ssh is, and have set it up before, you can
give your public key to github / gitlab and use the first command to avoid
typing in a password. If not, then just use the second command for now.

Regardless of which of the first two you ran, run the third one to push your
changes to the remote. After running one of the first two, you don't need to
run either of them again unless you're changing the remote location.

Additionally, you can just type git push from now on unless you change want to
change or add remotes or change the default remote that is pushed to
-->

---

## Pulling New Changes In

`git pull`

<!--
Assuming you've run the commands from the previous slide, you should be
able to run this command in a folder with a git repo without issue. It will
check any remotes you have set for new changes, and if there are changes,
attempt to automatically merge them in. This automatic merge will often times
fail, and figuring out how to deal with that is an important skill.
-->

---

## Cloning a Remote Repository

```bash
git clone git@github.com:username/reponame.git
git clone https://gitlab.com/username/reponame.git
```

<!--
the clone command is for grabbing an entire repo and its history and
putting a copy into a folder, which you can specify. If you've done ssh setup
before then you can use the first clone, otherwise use the second. In either
case, replace username with your username and reponame with the name of your
repository.
-->

---

## Where to Go From Here?

- [Git Documentation](https://git-scm.com/doc)
- [Merge Documentation](https://git-scm.com/docs/git-merge)
- [GitHub ssh key](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account)
- [GitLab ssh key](https://docs.gitlab.com/ee/user/ssh.html)
- [GPG key](https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key)

<!--
From here you know almost everything you need to to work on a project by yourself
with version control. The main thing we haven't had time to cover is how you can
actually reverse time to go back to previous commits. Worst case scenario for
now you can just browse the commit history in a web browser :)

To start working with others you're going to want to know
what a merge is, and how to do that. You'll also want to set up the ssh key
thing, as it makes your life much easier and you don't have to keep typing in
your password whenever you interact with a remote. Finally, you can generate a
gpg key to sign your commits. This will give them a "verified" tag when you
make commits, which allows people to be sure it was really you who made a
commit.
-->
