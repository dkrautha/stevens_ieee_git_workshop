# Stevens IEEE Git Workshop Spring 2023

If you're here, hi! This is the source of the Git workshop presentation that was
given in Spring 2023. The presentation is all standard markdown, which is then
turned into a pdf / website by [marp](https://marp.app/), a free and open source
program.

If you want to know what commands were used specifically, check the `justfile`
in the root of the project.
