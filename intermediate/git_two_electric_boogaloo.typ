#import "@preview/polylux:0.3.1": *
#import themes.metropolis: *

#show: metropolis-theme.with()

#set text(font: "Fira Sans", weight: "light", size: 20pt)
#show math.equation: set text(font: "Fira Math")
#set strong(delta: 100)
#set par(justify: true)
#show link: t => text(fill: blue)[
  #underline[#t]
]

#title-slide(
  author: [David Krauthamer],
  title: "Intermediate Git",
  // subtitle: "Subtitle",
  date: "November 13, 2023",
  // extra: "Extra",
)

#slide(title: "Table of contents")[
  #metropolis-outline
]

// off the top of my head:
// - making a new branch
// - committing to that branch
// - making a pull request
// - show them how a merge conflict would happen by merging another branch with conflicting changes
// - how to actually fix a merge conflict

#new-section-slide("Branches")

#slide(
  title: "What happens when you make a commit?",
)[
  #side-by-side[
    Commits contain the following information:
    - A message you typed
    - The author’s name and email address
    - The commit’s parents
    - A pointer to a snapshot
  ][
    #uncover(
      "2",
    )[
      #rect(
        fill: gray,
      )[
        A snapshot contains the information about the current state of the files you're
        tracking.
      ]
    ]
  ]
]

#slide(
  title: "An Example",
)[
#set align(center)
```bash
# make a local repo and put whatever you want in it
git init
touch README test.rb LICENSE
git add README test.rb LICENSE
git commit -m "Initial commit"
git log \
  --pretty="commit: %h%n parent: %p%n tree: %t%n author: %an%n committer: %cn%n"
  ```
#figure(image("./commit-and-tree.png"))
]

#slide(title: "Commits and Parents")[
  Make some commits and look at the log again!
  #figure(image("./commits-and-parents.png"))
]

#slide(title: "What is a Branch?")[
  - Branches are lightweight, movable pointers to one of these commits.
  - The default branch is called "master".
  #figure(image("./branch-and-history.png", height: 75%))
]

#slide(title: "Creating a New Branch")[
```bash
  # creates a new pointer to the same commit you're currently on
  git branch <branch-name>
  ```
#figure(image("./two-branches.png", height: 75%))
]

#slide(title: "So no HEAD?")[
```bash
git log --oneline --decorate

f30ab (HEAD -> master, testing) Commit message here!
34ac2 Fix bug #1328 - stack overflow under certain conditions
98ca9 Initial commit
  ```

#side-by-side[
  We're still pointing to master!

  We need to get HEAD pointing to the new branch.
][
  #figure(image("./head-to-master.png", height: 75%))
]

]

#slide(title: "Switching Branches")[
#side-by-side[
```bash
  git checkout <branch-name>
  # example
  git checkout testing
  git log --oneline --decorate
  ```
][
  #only("2")[
    HEAD now points to testing!
    #figure(image("./head-to-testing.png", height: 75%))
  ]
]
]

#slide(title: "Making Changes on the New Branch")[
#side-by-side[
```bash
# make any changes you like
nvim test.rb
# make a new commit
git commit -a -m "Make a change"
git log --oneline --decorate
```
][
  #only("2")[
    #figure(image("./advance-testing.png"))
  ]
]
]

#slide(title: "Back to Master")[
#side-by-side[
```bash
git checkout master
git log --oneline --decorate
```
][
  #only("2")[
    #figure(image("./checkout-master.png"))
  ]
]
]

#slide(title: "Let's Change History")[
#side-by-side[
```bash
# make some changes again
# in the same place in the file
nvim test.rb
# make a new commit
git commit -a -m "Make a change"
git log --oneline --decorate
```
][
  #only("2")[
    #figure(image("./advance-master.png"))
  ]
]
]

#slide(title: "View The Current State of Your Repo")[
#align(center)[
```bash
git log --oneline --decorate --graph --all

# Example output:
# * 0938c9e (HEAD -> testing) Change to hello rutgers
# | * be7a579 (master) Change hello world to hello stevens
# |/
# * 01886b5 Add line before hello world
# * 11770c9 Add another line of text
# * 728379e Initial commit
  ```
]
]

#new-section-slide("Merging and Merge Conflicts")

#slide(title: "Initiating a Merge")[
Let's merge the changes from testing #sym.arrow master.

First, move to the branch you want to merge into.

```bash
  git checkout master
  ```

Then, initiate the merge with the `merge` command.

```bash
  git merge <branch-name>
  # for our example
  git merge testing
  ```
]

#slide(title: "Do You Feel Lucky, Punk?")[
Depending on what you changed in each of your new commits:
1. The merge happened successfully!
2. There was a merge conflict, and Git is yelling at you.
Example output:

```bash
Auto-merging test.rs
CONFLICT (content): Merge conflict in test.rs
Automatic merge failed; fix conflicts and then commit the result.
  ```

]

#slide(title: "What The Files Looked Like")[
#side-by-side[
```rust
// from branch master
fn main() {
    println!("A line of text before?");
    println!("Hello, Stevens!");
    println!("Another line of text!");
}
    ```
][
```rust
// from branch testing
fn main() {
    println!("A line of text before?");
    println!("Hello, Rutgers!");
    println!("Another line of text!");
}
  ```
]
]

#slide(
  title: "What The Conflicted File Looks Like",
)[
#set align(center)
```rust
// what the file looks like now
fn main() {
    println!("A line of text before?");
<<<<<<< HEAD
    println!("Hello, Stevens!");
=======
    println!("Hello, Rutgers!");
>>>>>>> testing
    println!("Another line of text!");
}
``` \ \

```rust
// what the file looks like now
fn main() {
    println!("A line of text before?");
<<<<<<< HEAD // Start of what was already in master
    println!("Hello, Stevens!");
======= // End of what was already in master, start of what was in testing
    println!("Hello, Rutgers!");
>>>>>>> testing // End of what was in testing
    println!("Another line of text!");
}
```
]

#slide(title: "Resolving The Conflicts")[
#side-by-side[
  - Decide what you want to keep.
  - Remove the conflict markers.
  - Commit your changes.
][
#only(2)[
```rust
fn main() {
    println!("A line of text before?");
    println!("Hello, Stevens!");
    println!("Another line of text!");
}
```
]
]
]

#new-section-slide("Sources and Further Reading")

#slide(
  title: "Sources and further reading",
)[
  - Credit for images and general roadmap: https://git-scm.com/book/en/v2
    - Also a fantastic resource for learning Git straight from the authors of the
      software!
  - Explanations for some confusing git terms:\
    https://jvns.ca/blog/2023/11/01/confusing-git-terminology/
  - The dark art of rebasing: https://git-scm.com/book/en/v2/Git-Branching-Rebasing
  - Overview of the general flow most companies have with branching:\
    https://docs.github.com/en/get-started/quickstart/github-flow
]
